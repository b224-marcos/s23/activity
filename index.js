/*
	OBJECTS
		- An object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Object Literals
		- one of the methods in creating objects.

		Syntax:
			let objectName = {
				keyA: valueA,
				keyB: valueB
			};
 
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating object using Object Literals: ");
console.log(cellphone);
console.log(typeof cellphone);


// Creating Objects Using a Constructor Function (Object Constructor)
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instance/copies of an object.


	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA;
			this.keyB = valueB
		};
	
		let variableName = new function ObjectName(valueA, valueB);
		console.log(variableName);

		=========================================
			- this is for invoking; it refers to the global object.
			- don't forget the "new" keyword when creating a new object.

*/

	// We use PascalCase for the Constructor Function Name.
	// template
	function Laptop (name, manufactureDate) {
		this.name = name
		this.manufactureDate = manufactureDate
	};

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using Constructor Function:");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", [2020, 2021]);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result of creating objects without the new keyword:");
console.log(oldLaptop);



// Creating empty objects as placeholder.
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate: 2012
};

console.log(myComputer);


/*
	Mini Activity:
		- Create an object constructor function to produce 2 objects with 3 key-value pairs.
		- Log the 2 new objects in the console and send SS in our batch hangout.

*/

// Solution

function Camera (brand, pixels, manufactureDate) {
	this.brand = brand
	this.pixels = pixels
	this.manufactureDate = manufactureDate
};

let myCamera = new Camera("Nikon", "40MP", 2019);
let myFriendsCamera = new Camera("Canon", "20MP", 2008);
console.log("New objects created using object constructor:");
console.log(myCamera);
console.log(myFriendsCamera);


// Accessing Object Properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name);

// Using the square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"]);



// Accessing array of objects
let deviceArr = [laptop, myLaptop];
// let deviceArr = [{name: Lenovo, manufactureDate: 2008}, {name: Macbook Air, manufactureDate: 2020}];

// Dot Notation
console.log(deviceArr[0].manufactureDate);

// Square Backet Notation
console.log(deviceArr[0]["manufactureDate"]);



// Initializing/Adding/Deleting/Reassigning Object Properties
// (CRUD Operations)

// Initializing Object
let car = {};
console.log(car);


// Adding Object Properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation:");
console.log(car);

car["manufacture date"] = 2019;
console.log("Result from adding property using square bracket notation:");
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting object properties:");
console.log(car);

// Reassigning object properties (update)
car.name = "Tesla";
console.log("Result from reassigning property:");
console.log(car);

// Object Methods

/*
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/


let person = {
	name: "John",
	age: 25,
	talk: function() {
		console.log("Hello! My Name is " + this.name)
	}
};

console.log(person);
console.log("Result from Object Method: ");
person.talk();

person.walk = function() {
	console.log(this.name + " have walked 25 steps forward.")
};

person.walk();




let friend = {
	firstName: "Jane",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["janedoe@mail.com", "jane121992@gmail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName + "." + "I live in " + this.address.city + ", " + this.address.country + ".")
	}
};


friend.introduce();


// Real World Application Objects


// Using Object Literals
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 15,
	tackle: function() {
		console.log(this.name + " tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function() {
		console.log(this.name + " fainted.")
	}
};

console.log(myPokemon);
myPokemon.tackle();


// Creating real world object using constructor function
function Pokemon(name, level) {

	// Propeties
	this.name = name
	this.level = level
	this.health = 5 * level;
	this.attack = 2 * level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = target.health - this.attack;

		if (target.health <= 5){
			console.log(target.name + ' Fainted!')
		}
	};

		this.faint = function() {
		console.log(this.name + " fainted.")
	}
};

let charmander = new Pokemon("Charmander", 12);
let squirtle = new Pokemon("Squirtle", 6);

console.log(charmander);
console.log(squirtle);

charmander.tackle(squirtle);
charmander.tackle(squirtle);

/*

1. Create a new set of pokemon for pokemon battle. (same as our discussion) 
 - solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
 (target.health - this.attack)

 2. If health is less than or equal to 5, invoke faint function


*/




let haunter = new Pokemon("Haunter", 12);
let snorlax = new Pokemon("Snorlax", 8);



haunter.tackle(snorlax);
snorlax.tackle(haunter);
haunter.tackle(snorlax)







